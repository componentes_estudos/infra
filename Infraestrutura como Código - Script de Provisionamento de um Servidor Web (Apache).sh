#!/bin/bash

echo "Atualizando o servidor... "
apt-get update
apt-get upgrade -y
apt-get install apache2 -y
apt-get install unzip -y

echo "Baixando e copiando os arquivos da aplicação"
cd /tmp
wget <repo_arquivos/App>
unzip main.zip
cd <nome_do_diretório>
cp -R ./* /var/www/html